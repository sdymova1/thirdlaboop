#include <iostream>
#include <SFML/Graphics.hpp>
using namespace std;


class Point {
public:
    float x, y;
    float r = 10;

    void draw(sf::RenderWindow &window) const {
        sf::CircleShape circle(r);
        circle.setPosition(x-r, y-r);
        circle.setFillColor(sf::Color(100, 5, 5));
        window.draw(circle);
    }

    bool operator<(Point p) const
    {
        return x < p.x || (x == p.x && y < p.y);
    }
};

float cross_product(Point A, Point B, Point C) {
    return (B.x - A.x) * (C.y - A.y)
           - (B.y - A.y) * (C.x - A.x);
}

vector<Point> convex_hull(vector<Point> points){
    int n = points.size();
    vector<Point> shell, above, under;

    if (n <= 3)
        return points;

    sort(points.begin(), points.end());
    vector<Point> line;
    line = {points[0], points[n-1]};
    shell.push_back(line[0]);

    for (int i = 1; i < n-1; i++ ) {
        if (cross_product(line[0], line[1], points[i]) < 0) {
            above.push_back(points[i]);
        }
        else if (cross_product(line[0], line[1], points[i]) > 0) {
            under.push_back(points[i]);
        }
    }

    Point a = line[0];

    for (int i = 0; i < above.size(); i++ ) {
        for (int j = 0; j < above.size(); j++ ) {
            if (cross_product(a, above[j], above[i]) >= 0) {
                continue;
            }
            shell.push_back(above[i]);
            a = above[i];
        }

    }

    shell.push_back(line[1]);

    for (int i = 0; i < under.size(); i++ ) {
        for (int j = 0; j < under.size(); j++ ) {
            if (cross_product(a, under[j], under[i]) >= 0) {
                continue;
            }
            shell.push_back(under[i]);
            a = under[i];
        }
    }

    return shell;
}

int main()
{
    vector<Point> points = {
            {100, 90},
            {666, 111},
            {30, 444},
            {500, 800},
            {790, 500},
            {500, 500},
            {300, 22},
            {244, 444},
            {700, 299}
    };


    vector<Point> shell = convex_hull(points);

    sf::ConvexShape conv;
    conv.setPointCount(shell.size());
    for (int i = 0; i < shell.size(); i++) {
        conv.setPoint(i, sf::Vector2f(shell[i].x, shell[i].y));
    }
    conv.setFillColor(sf::Color(100, 200, 200));
    conv.setOutlineThickness(5);
    conv.setOutlineColor(sf::Color(100, 5, 5));



    sf::RenderWindow window(sf::VideoMode(900, 900),
                            "Smth works, I guess");


    while (window.isOpen())
    {
        sf::Event event{};
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }


        window.clear(sf::Color(100, 200, 200));

        window.draw(conv);

        for (auto p:points) {
            p.draw(window);
        }

        window.display();
    }
}