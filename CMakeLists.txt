cmake_minimum_required(VERSION 3.17)
project(third_lab)

set(CMAKE_CXX_STANDARD 14)
find_package(SFML 2.5 COMPONENTS system window graphics)

add_executable(third_lab main.cpp)
target_link_libraries(third_lab sfml-system sfml-window sfml-graphics)